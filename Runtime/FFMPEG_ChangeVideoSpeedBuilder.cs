﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FFMPEG_ChangeVideoSpeedBuilder : FFMPEGCommandBuilderMono<FFMPEGChangeVideoSpeed>
{

    public string m_relativePathToSpeedUp = "Export\\Origin.mp4";
    public string m_relativePathResult = "Export\\OriginLoop.mp4";
    public float m_speedUpMultiplicator = 2;

    protected override FFMPEGChangeVideoSpeed GetPrebuildCommandOfChildren()
    {
        FFMPEGChangeVideoSpeed convertInfo = new FFMPEGChangeVideoSpeed();
        convertInfo.SetVideoToSpeedUp(
            GetProjectRootPath() + "\\" + m_relativePathToSpeedUp);
        convertInfo.SetResultPath(
            GetProjectRootPath() + "\\" + m_relativePathResult);
        convertInfo.SpeedupTheVideo(m_speedUpMultiplicator);
        return convertInfo;
    }
}






