﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FFMPEG_Video2VideoCycles : FFMPEGCommandBuilderMono<FFMPEGConvertionVideo2VideoCycle>
{
    //Source: https://video.stackexchange.com/questions/12905/repeat-loop-input-video-with-ffmpeg

    public string m_pathOrigineVideoFile = "Export\\Origin.mp4";
    public string m_pathExportVideoFile = "Export\\OriginLoop.mp4";
    public float m_maxTimeToLoop = 36;


    
    protected override FFMPEGConvertionVideo2VideoCycle GetPrebuildCommandOfChildren()
    {
        FFMPEGConvertionVideo2VideoCycle convertInfo = new FFMPEGConvertionVideo2VideoCycle();
        convertInfo.SetVideoFileOriginPath(
            GetProjectRootPath() + "\\" + m_pathOrigineVideoFile);
        convertInfo.SetVideoFileExportPath(
            GetProjectRootPath() + "\\" + m_pathExportVideoFile);
        convertInfo.SetMaxTimeWithSeconds(m_maxTimeToLoop);
        return convertInfo;
    }
}
[System.Serializable]
public class FFMPEGConvertionVideo2VideoCycle : FFMPEGCommand
{
    //ffmpeg -t 36 -stream_loop -1 -i A.mp4 -c copy B.mp4 q
    public float m_maxTime = 36;
    public string m_absoluePathOrigineVideo = "..\\Origine.mp4";
    public string m_absolutePathExportVideo = "..\\DefaultExport.mp4";

    public override string GetCommandLine()
    {

        return string.Format(" -y -t {0} -stream_loop -1 -i {1} -c copy {2}",
            m_maxTime, m_absoluePathOrigineVideo, m_absolutePathExportVideo);
    }
    public void SetMaxTimeWithFPS(int frames, int fps = 60) { m_maxTime = (float)frames / (float)fps; }
    public void SetMaxTimeWithSeconds(float timeInSeconds) { m_maxTime = timeInSeconds; }
    public void SetVideoFileOriginPath(string absolutePath) { m_absoluePathOrigineVideo = absolutePath; }
    public void SetVideoFileExportPath(string absolutePath) { m_absolutePathExportVideo = absolutePath; }

    public override void CheckAndCreateFolderStructure()
    {
        IfDirectoryDontExisteCreateIt(Path.GetDirectoryName(m_absolutePathExportVideo));
        IfDirectoryDontExisteCreateIt(Path.GetDirectoryName(m_absoluePathOrigineVideo));
    }
}