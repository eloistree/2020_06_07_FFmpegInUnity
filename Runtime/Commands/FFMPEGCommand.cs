﻿
using JetBrains.Annotations;
using System;
using System.IO;
using UnityEngine;

public abstract class FFMPEGCommand
{
    [SerializeField] bool m_useDebugReturn;
    [SerializeField] string m_ffmpegBinAbsolutePath;


    public FFMPEGCommand()
    {
        SetWithDefaultFFMPEGPath();
    }
    public FFMPEGCommand(string ffmpegAbsolutePath)
    {
        SetPathBinFFMPEG(ffmpegAbsolutePath);
    }


    public bool IsFFMPEGPathDefined() { return m_ffmpegBinAbsolutePath != null && m_ffmpegBinAbsolutePath.Length > 0; }
    public bool IsFFMPEGPFolderExist() { return IsFFMPEGPathDefined() && Directory.Exists(GetFFMPEGAbsolutePath()); }

    public string GetFFMPEGAbsolutePath()
    {
        return m_ffmpegBinAbsolutePath;
    }
    public void SetWithDefaultFFMPEGPath()
    {
        SetPathBinFFMPEG(FFMPEGUtility.GetDefaultAbsolutePathFFMPEG());
    }

    public void SetDebugUse(bool useDebug)
    {
        m_useDebugReturn = useDebug;
    }

    public void SetPathBinFFMPEG(string absolutePath)
    {
        m_ffmpegBinAbsolutePath = absolutePath;
    }
    public abstract string GetCommandLine();
    public string GetCmdReadyCommandLine()
    {
        return string.Format("\"{0}\\ffmpeg\" {1}",
           SlashToBackSlash(m_ffmpegBinAbsolutePath), GetCommandLine());
    }
    public static string SlashToBackSlash(string text)
    {
        return text.Replace("/", "\\");
    }
    public void Execute()
    {
        CheckAndCreateFolderStructure();
        FFMPEGUtility.Execute(this, m_useDebugReturn);
    }
    public abstract void CheckAndCreateFolderStructure();
    public void IfDirectoryDontExisteCreateIt(string directoryAbsolutePath) {

        try
        {
            if (!Directory.Exists(directoryAbsolutePath))
                Directory.CreateDirectory(directoryAbsolutePath);
        }
        catch (Exception e) { Debug.LogError("Did not created directory:"+ directoryAbsolutePath +"\n"+ e.StackTrace); }

    }
}