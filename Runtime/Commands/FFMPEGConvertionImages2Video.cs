﻿
using System.IO;
using UnityEngine;

[System.Serializable]
public class FFMPEGConvertionImages2Video : FFMPEGCommand
{

    //ffmpeg\ffmpeg\bin -r 60 -f image2 -s 1920x1080 -i C:\...\UnityScreenshot\Screenshot_%04d.PNG -vcodec libx264 -crf 25  -pix_fmt yuv420p video.mp4

    [SerializeField] int m_widthResolution = 1920;
    [SerializeField] int m_heightResolution = 1080;
    [SerializeField] string m_imagesDirectoryAbolutePath = "";
    [SerializeField] string m_imagesFormat = "Image_%04d.PNG";
    [SerializeField] int m_framesPerSecond = 60;
    [SerializeField] string m_exportDirectoryAbolutePath = "";
    [SerializeField] string m_exportVideoName = "DefaultExport.mp4";


    public override string GetCommandLine()
    {
        string usePathImage = GetImagesFormatAbstractPath();
        string usePathVideo = GetExportVideoAbstractPath();
        return string.Format(" -y -r {0} -f image2 -s {1}x{2} -i {3} -vcodec libx264 -crf 25  -pix_fmt yuv420p {4}",
            m_framesPerSecond, m_widthResolution, m_heightResolution, usePathImage, usePathVideo);
    }

    private string GetExportVideoAbstractPath()
    {
        string usePathVideo = m_exportVideoName;
        if (!string.IsNullOrEmpty(m_exportDirectoryAbolutePath))
        {
            usePathVideo = m_exportDirectoryAbolutePath + "\\" + usePathVideo;
        }

        return usePathVideo;
    }

    private string GetImagesFormatAbstractPath()
    {
        string usePathImage = m_imagesFormat;
        if (!string.IsNullOrEmpty(m_imagesDirectoryAbolutePath))
        {
            usePathImage = m_imagesDirectoryAbolutePath + "\\" + usePathImage;
        }

        return usePathImage;
    }

    public void SetImageFormatIndexAtEnd(string startFormatText = "Image_", string indexFormat = "%04d", string imageFormat = ".png")
    {
        m_imagesFormat = startFormatText + indexFormat + imageFormat;
    }
    public void SetImageFormatIndexInFront(string indexFormat = "%04d", string endFormatText = "_Image", string imageFormat = ".png")
    {
        m_imagesFormat = indexFormat + endFormatText + imageFormat;
    }
    public void SetFPS(int fps) { m_framesPerSecond = fps; }
    public void SetResolution(int width, int height) { m_widthResolution = width; m_heightResolution = height; }
    public void SetDirectoryPathImage(string absolutePath) { m_imagesDirectoryAbolutePath = absolutePath; }
    public void SetDirectoryPathExport(string absolutePath) { m_exportDirectoryAbolutePath = absolutePath; }

    public void SetExportFileName(string videoNameWithExtension)
    {
        m_exportVideoName = videoNameWithExtension;
    }

    public override void CheckAndCreateFolderStructure()
    {
        IfDirectoryDontExisteCreateIt(m_exportDirectoryAbolutePath);
        IfDirectoryDontExisteCreateIt(m_imagesDirectoryAbolutePath);
    }
}