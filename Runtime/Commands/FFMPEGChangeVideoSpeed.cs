﻿using System.IO;
using UnityEngine;

[System.Serializable]
public class FFMPEGChangeVideoSpeed : FFMPEGCommand
{
    [SerializeField] string m_absolutePathToSpeedUp = "..\\Video.mp4";
    [SerializeField] string m_absolutePathResult = "..\\VideoSpeedUp.mp4";
    [SerializeField] float m_speed = 2f;
    public override string GetCommandLine()
    {
        return string.Format("-i {0} -filter:v \"setpts = {1} * PTS\" {2}",
            m_absolutePathToSpeedUp, m_speed, m_absolutePathResult
           );
    }
    public void SetVideoToSpeedUp(string absolutePath)
    {
        m_absolutePathToSpeedUp = absolutePath;
    }
    public void SetResultPath(string absolutePath)
    {
        m_absolutePathResult = absolutePath;
    }
    public void SlowTheVideo(float multiplicator)
    {
        m_speed = multiplicator;
    }
    public void SpeedupTheVideo(float multiplicator)
    {
        m_speed = 1f / multiplicator;
    }

    public override void CheckAndCreateFolderStructure()
    {
        IfDirectoryDontExisteCreateIt(Path.GetDirectoryName(m_absolutePathResult));
    }
}