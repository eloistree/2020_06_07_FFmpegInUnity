﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FFMPEG_Manual : MonoBehaviour
{

    public void OpenDownloadPage() {Application.OpenURL("https://www.ffmpeg.org/download.html"); }
    public void OpenManualPage() { Application.OpenURL("https://www.ffmpeg.org/documentation.html"); }
}
