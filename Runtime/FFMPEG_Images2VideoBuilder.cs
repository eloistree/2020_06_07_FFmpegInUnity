﻿using System;

public class FFMPEG_Images2VideoBuilder : FFMPEGCommandBuilderMono<FFMPEGConvertionImages2Video>
{
    public string m_pathImagesFolder = "Images";
    public string m_pathExportFolder = "Export";
    public string m_videoExportName = "DefaultExport.mp4";
    public string m_imageStart = "Image_";
    public string m_imageExtension = ".png";
    public int m_framesPerSecond = 60;

    public void SetExportName(string name){ m_videoExportName = name + ".mp4";  }
    public void SetImageStartNameFormat(string name){ m_imageStart = name; }

    protected override FFMPEGConvertionImages2Video GetPrebuildCommandOfChildren()
    {
        FFMPEGConvertionImages2Video convertInfo = new FFMPEGConvertionImages2Video();
        convertInfo.SetDirectoryPathImage(
            GetProjectRootPath() + "\\" + m_pathImagesFolder);
        convertInfo.SetDirectoryPathExport(
             GetProjectRootPath() + "\\" + m_pathExportFolder);
        convertInfo.SetImageFormatIndexAtEnd(m_imageStart, "%04d", m_imageExtension);
        convertInfo.SetExportFileName(m_videoExportName);
        convertInfo.SetFPS(m_framesPerSecond);
        return convertInfo;
    }

    public void SetImagesExtensions(string extensions)
    {
        m_imageExtension = extensions;
    }

    public void SetExportRelativeFolderPath(string relativeFolderPath)
    {
        m_pathExportFolder = relativeFolderPath;
    }

    public void SetImportImagesRelativePath(string relativeFolderPath)
    {
        m_pathImagesFolder = relativeFolderPath;
    }
}

