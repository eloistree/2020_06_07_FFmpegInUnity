﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FFMPEGUtility
{
    private static string m_relativePathFFMPEGInProjectRoot = "\\ffmpeg\\bin";
    public static void Execute(FFMPEGCommand command, bool useDebug) {

        ExecuteWindowCommands.RunCommands(command.GetCmdReadyCommandLine(), null, useDebug);
    }
    public static string GetDefaultAbsolutePathFFMPEG()
    {
        return GetUnityProjectRootPath() + m_relativePathFFMPEGInProjectRoot;
    }
    public static string HasFFMPEGInUnityRoot()
    {
        return GetDefaultAbsolutePathFFMPEG();
    }
    public static string GetUnityProjectRootPath()
    {
        return Directory.GetCurrentDirectory();
    }
}
public abstract class FFMPEGCommandBuilderMono<T>  : MonoBehaviour where T:FFMPEGCommand
{
   
    [Header("Debug View")]
    [TextArea(0, 8)]
    [SerializeField] protected string m_prebuildFormat;

    public string GetProjectRootPath() { 
        return Directory.GetCurrentDirectory();
    }

    public T GetPrebuildCommand() {
        T cmd = GetPrebuildCommandOfChildren();
        RefreshDebug(cmd);
        return cmd;
    }

    protected abstract T GetPrebuildCommandOfChildren();

    private void OnValidate()
    {
        RefreshDebug();
    }

    private void RefreshDebug()
    {
        m_prebuildFormat = GetPrebuildCommandOfChildren().GetCmdReadyCommandLine();
    }
    private void RefreshDebug(T cmd)
    {
        m_prebuildFormat = cmd.GetCmdReadyCommandLine();
    }


    public void ExecuteWithCurrentPrebuild() {
        GetPrebuildCommand().Execute();
    }


}

