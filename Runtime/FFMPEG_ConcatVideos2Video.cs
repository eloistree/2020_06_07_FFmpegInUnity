﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using UnityEngine;

public class FFMPEG_ConcatVideos2Video : FFMPEGCommandBuilderMono<FFMPEGConcatVideos2VideoWithLoop>
{


    public string m_videosDirectoryPath = "Export\\";
    public string m_outputVideoMergePath = "Export\\Merge\\MergeResult.mp4";
    public int m_loopByVideo = 2;
    public int m_loopAllVideo = 2;



    protected override FFMPEGConcatVideos2VideoWithLoop GetPrebuildCommandOfChildren()
    {
        FFMPEGConcatVideos2VideoWithLoop convertInfo = new FFMPEGConcatVideos2VideoWithLoop();
        convertInfo.SetVideoMergedResult(
            GetProjectRootPath() + "\\" + m_outputVideoMergePath);
        convertInfo.SetVideosFolderToMerge(
           GetProjectRootPath() + "\\" + m_videosDirectoryPath);
        convertInfo.SetLoops(m_loopByVideo, m_loopAllVideo);
        return convertInfo;
    }
    public void OpenOrigineVideosDirectory() { GetPrebuildCommandOfChildren().OpenOrigineVideosDirectory(); }
    public void OpenExportVideoDirectory() { GetPrebuildCommandOfChildren().OpenExportVideoDirectory(); }

}

[System.Serializable]
public class FFMPEGConcatVideos2VideoWithLoop : FFMPEGCommand
{
    //ffmpeg -f concat -i file.txt -c copy "output.mp4"
    public string m_pathFolderVideos = "..\\Export";
    public string m_pathToFusionVideoFile = "..\\Export\\Merge\\FusionResult.mp4";
    public SearchOption m_exploreOption = SearchOption.TopDirectoryOnly;
    public int m_loopByVideo = 2;
    public int m_loopAllVideo = 2;

    public void SetLoops(int byVideo = 2, int byFullList = 1)
    {
        m_loopByVideo = byVideo;
        m_loopAllVideo = byFullList;
    }

    public string GetTempFusionFilePath()
    {
        return Application.persistentDataPath + "\\tmp.txt";
    }

    public void TransfertVideosToListFile()
    {

        CheckThatFileAndFoldersExist();
        string tmpPath = GetTempFusionFilePath();
        string[] videosPath = Directory.GetFiles(m_pathFolderVideos, "*.mp4", m_exploreOption);
        List<string> pathLoops = new List<string>();

        for (int y = 0; y < m_loopAllVideo; y++)
        {
            for (int i = 0; i < videosPath.Length; i++)
            {
                for (int j = 0; j < m_loopByVideo; j++)
                {
                    pathLoops.Add(videosPath[i]);

                }
            }
        }



        for (int i = 0; i < pathLoops.Count; i++)
        {
            // videosPath[i] = "\"" + videosPath[i] + "\"";
            pathLoops[i] = "file '" + pathLoops[i] + "'";
        }

        File.WriteAllText(tmpPath, string.Join("\n", pathLoops));
    }
    public void CheckThatFileAndFoldersExist()
    {

        string pTmp = Path.GetDirectoryName(m_pathFolderVideos);
        if (!Directory.Exists(pTmp))
            Directory.CreateDirectory(pTmp);
        pTmp = Path.GetDirectoryName(m_pathToFusionVideoFile);
        if (!Directory.Exists(pTmp))
            Directory.CreateDirectory(pTmp);
        string filePath = GetTempFusionFilePath();
        if (!File.Exists(filePath))
            File.WriteAllText(filePath, "");
    }

    public override string GetCommandLine()
    {

        return string.Format("-y -safe 0 -f concat -i \"{0}\" -c copy \"{1}\"",
           SlashToBackSlash(GetTempFusionFilePath()), SlashToBackSlash(m_pathToFusionVideoFile));
    }

    public void SetVideosFolderToMerge(string absolutePath) { m_pathFolderVideos = absolutePath; }
    public void SetVideoMergedResult(string absolutePath) { m_pathToFusionVideoFile = absolutePath; }

    public override void CheckAndCreateFolderStructure()
    {
        IfDirectoryDontExisteCreateIt(m_pathFolderVideos);
        IfDirectoryDontExisteCreateIt(Path.GetDirectoryName(m_pathToFusionVideoFile));
        TransfertVideosToListFile();
    }

    public void OpenOrigineVideosDirectory() { Application.OpenURL(m_pathFolderVideos); }
    public void OpenExportVideoDirectory() { Application.OpenURL(Path.GetDirectoryName(m_pathToFusionVideoFile)); }
}